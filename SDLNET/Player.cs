﻿using SdlDotNet.Core;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Input;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Font = SdlDotNet.Graphics.Font;

namespace SDLNET
{
    public class Player
    {
        // Pozycja bezwzględna (px)
        public int x = 0;
        public int y = 0;

        // Rozmiar postaci (px)
        public int width = 20;
        public int height = 20;
        
        // Pozycje na planszy (numer pola)
        public int positionX = 0; 
        public int positionY = 0;

        public int currentMove = (int)CursorKey.UP; // 0 - góra, 1 - prawo, 2 - dół, 3 - lewo
        public int currentAnimation = 0;

        public string name = null;

        public Key upKey = Key.UpArrow;
        public Key downKey = Key.DownArrow;
        public Key leftKey = Key.LeftArrow;
        public Key rightKey = Key.RightArrow;
        public Key fireKey = Key.RightControl;

        public bool[] cursorKey = new bool[5];


        private float mleftover = 0;

        public Surface texture;
        public Surface explosionSurface;
        public Rectangle ghostElement;
        public int textureID = 0;
        
        public Player opponent;

        public Surface text;
        private ClassicMap map;
        public Shoot shoot;

        public bool isExplosion = false;
        public int[] explosionAnimate = new int[] {0,0};

        public void setOpponent(Player opponent)
        {
            this.opponent = opponent;
        }

        public Player(string name, Point startPoint, ref ClassicMap map)
        {
            shoot = new Shoot(this);
            x = startPoint.X;
            y = startPoint.Y;
            this.name = name;
            this.map = map;
            ghostElement = new Rectangle(new Point(x,y), new Size(30,30));
        }
        
        public void initialize(Surface surface)
        {
            this.text = (new Font("Arial.ttf", 12)).Render(this.name, Color.Yellow);
            //Wczytywanie tekstur
            
            this.texture = surface;
            this.texture.Transparent = true;
            this.texture.TransparentColor = Color.FromArgb(255, 0, 255);

            this.explosionSurface = (new Surface(@"resources\explosion.png")).Convert(SDLGame.m_VideoScreen, true, false);
            this.explosionSurface.Transparent = true;
            this.explosionSurface.TransparentColor = Color.FromArgb(255, 0, 255);

            shoot.init();
        }

        public int getGridPool(int number)
        {
            return (int)number / 32;
        }

        private bool isCollisionWithMapElements(CursorKey direction)
        {
            int tmpX = 0, tmpY = 0, tmpX2 = 0, tmpY2 = 0;
            if (direction == CursorKey.UP)
            {
                tmpX = getGridPool(this.x);
                tmpY = getGridPool(this.y - 1);

                tmpX2 = getGridPool(this.x + 30);
                tmpY2 = getGridPool(this.y - 1);
            }
            else if (direction == CursorKey.DOWN)
            {
                tmpX = getGridPool(this.x);
                tmpY = getGridPool(this.y + 30 + 1);

                tmpX2 = getGridPool(this.x + 30);
                tmpY2 = getGridPool(this.y + 30 + 1);
            }
            else if (direction == CursorKey.LEFT)
            {
                tmpX = getGridPool(this.x - 1);
                tmpY = getGridPool(this.y);

                tmpX2 = getGridPool(this.x - 1);
                tmpY2 = getGridPool(this.y + 30);
            }
            else if (direction == CursorKey.RIGHT)
            {
                tmpX = getGridPool(this.x + 30 + 1);
                tmpY = getGridPool(this.y);

                tmpX2 = getGridPool(this.x + 30 + 1);
                tmpY2 = getGridPool(this.y + 30);
            }

            if (SDLGame.map.grid[tmpY, tmpX] == 1 || SDLGame.map.grid[tmpY2, tmpX2] == 1)
                return true;

            return false;
        }

        private bool isCollisionWithOpponent(CursorKey direction)
        {
            Point tmpPosiotion = new Point();
            Rectangle tmpGhost;

            if (direction == CursorKey.UP)
            {
                tmpPosiotion = new Point(x, y - 1);
            }
            else if (direction == CursorKey.DOWN)
            {
                tmpPosiotion = new Point(x, y + 1);
            }
            else if (direction == CursorKey.LEFT)
            {
                tmpPosiotion = new Point(x-1, y);
            }
            else if (direction == CursorKey.RIGHT)
            {
                tmpPosiotion = new Point(x+1, y);
            }

            tmpGhost = new Rectangle(tmpPosiotion, new Size(30, 30));
            if (opponent.ghostElement.IntersectsWith(tmpGhost))
            {
                return true;
            }
            return false;
        }

        private bool isCollision(CursorKey direction)
        {
            if (isCollisionWithOpponent(direction) || isCollisionWithMapElements(direction))
            {
                return true;
            }
            else {
                return false;
            }
        }
        CursorKey lasrDir = CursorKey.UP;
        // Wszelkiego typu operacje wykonywane w czasie jednego cyklu (animacje, ruch obliczenia etc.)
        // EventHandler<KeyboardEventArgs>
        public void logic(TickEventArgs tickArgs)
        {
            // Ruch postaci
            int offsetPerSecond = 150; // Przesunięcie postaci w jednej sekundzie
            float offset = 1.0f / 1000 * tickArgs.TicksElapsed * offsetPerSecond;
            if (this.mleftover < 1)
            {
                offset += this.mleftover;
                this.mleftover = 0;
            }

            if (offset < 1)
            {
                this.mleftover += offset;
                offset = 0;
            }

            
            if(this.cursorKey[0] || this.cursorKey[1] || this.cursorKey[2] || this.cursorKey[3])
                this.currentAnimation = (this.currentAnimation + (int)offset) % 8;

            if (this.cursorKey[(int)CursorKey.UP])
            {
                lasrDir = CursorKey.UP;
                if(!isCollision(CursorKey.UP))
                    this.y -= (int)offset;
                this.currentMove = (int)CursorKey.UP;
            }
            else if (this.cursorKey[(int)CursorKey.DOWN])
            {
                lasrDir = CursorKey.DOWN;

                if (!isCollision(CursorKey.DOWN))
                    this.y += (int)offset;
                this.currentMove = (int)CursorKey.DOWN;
            }
            else if (this.cursorKey[(int)CursorKey.LEFT])
            {
                lasrDir = CursorKey.LEFT;
                if (!isCollision(CursorKey.LEFT))
                    this.x -= (int)offset;
                this.currentMove = (int)CursorKey.LEFT;
            }
            else if (this.cursorKey[(int)CursorKey.RIGHT])
            {
                lasrDir = CursorKey.RIGHT;
                if (!isCollision(CursorKey.RIGHT))
                    this.x += (int)offset;
                this.currentMove = (int)CursorKey.RIGHT;
            }

            if(this.cursorKey[ (int)CursorKey.FIRE ])
                this.shoot.fire((CursorKey)currentMove); 
            

            ghostElement.Y = y;
            ghostElement.X = x;


            this.draw();
        }        

        public void draw()
        {
            // Wyświetlanie nazwy gracza
            SDLGame.m_VideoScreen.Blit( this.text, new Point(this.x+16 - this.text.Width/2, this.y-18));
            // Wyświetlanie obiektu
            SDLGame.m_VideoScreen.Blit(texture, new Point(this.x, this.y), new Rectangle(new Point(32 * currentAnimation, 32 * currentMove), new Size(32, 32)));

            if (this.isExplosion)
                this.explosion();

        }

        public void explosion()
        {
            if(!this.isExplosion)
                this.isExplosion = true;

            SDLGame.m_VideoScreen.Blit(explosionSurface, new Point(this.x, this.y), new Rectangle(new Point(32 * this.explosionAnimate[0], 32 * this.explosionAnimate[1]), new Size(32, 32)));

            if (this.explosionAnimate[0] < 5)
                this.explosionAnimate[0]++;
            else
            {
                this.explosionAnimate[1]++;
                this.explosionAnimate[0] = 0;
            }

            if (this.explosionAnimate[0] == 5 && this.explosionAnimate[1] == 5)
            {
                this.isExplosion = false;
                this.explosionAnimate[0] = 0;
                this.explosionAnimate[1] = 0;
            }
        }
        
        // Metoda obsługująca wciśnięte klawisze
        public void movement(KeyboardEventArgs keyArgs)
        {
            if (keyArgs.Key == this.upKey)
            {
                this.cursorKey[0] = keyArgs.Down;
            }
            else if (keyArgs.Key == this.downKey)
            {
                this.cursorKey[2] = keyArgs.Down;
            }
            else if (keyArgs.Key == this.leftKey)
            {
                this.cursorKey[3] = keyArgs.Down;
            }
            else if (keyArgs.Key == this.rightKey)
            {
                this.cursorKey[1] = keyArgs.Down;
            }

            if (keyArgs.Key == this.fireKey)
            {
                this.cursorKey[4] = keyArgs.Down;
            }
        }

        
    }
}
