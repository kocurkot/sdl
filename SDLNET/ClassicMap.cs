﻿using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SDLNET
{
    public class ClassicMap
    {
        public int[,] grid = new int[,] {
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,0,1,1,1,1,0,0,0,0,0,1,1,1,0,0,0,0,0,1,1,1,1,0,1},
            {1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1},
            {1,0,1,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1,0,1},
            {1,0,1,0,0,1,1,0,0,0,1,0,0,0,1,0,0,0,1,1,0,0,1,0,1},
            {1,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,1},
            {1,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,1},
            {1,0,1,1,0,0,1,1,0,0,0,0,1,0,0,0,0,1,1,0,0,1,1,0,1},
            {1,0,1,1,0,0,1,1,0,0,0,0,1,0,0,0,0,1,1,0,0,1,1,0,1},
            {1,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,1},
            {1,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,1},
            {1,0,1,0,0,1,1,0,0,0,1,0,0,0,1,0,0,0,1,1,0,0,1,0,1},
            {1,0,1,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1,0,1},
            {1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1},
            {1,0,1,1,1,1,0,0,0,0,0,1,1,1,0,0,0,0,0,1,1,1,1,0,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
        };

        public int gridWidth = 32;
        public int gridHeight = 32;

        public Surface[] textures = new Surface[2];

        public void initialize()
        {
            //Wczytywanie tekstur
            this.textures[0] = (new Surface(@"resources\0.png")).Convert(SDLGame.m_VideoScreen, true, false);
            this.textures[1] = (new Surface(@"resources\1.png")).Convert(SDLGame.m_VideoScreen, true, false);
        }

        // Rysowanie mapy
        public void draw()
        {
            for (int y = 0; y < 18; y++)
            {
                for (int x = 0; x < 25; x++)
                {
                    SDLGame.m_VideoScreen.Blit(this.textures[grid[y, x]], new Point(x * 32, y * 32));
                }
            }
        }

    }
}
