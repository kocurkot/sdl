﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDLNET
{
    public enum CursorKey : int
    {
        UP = 0,
        RIGHT = 1,
        DOWN = 2,
        LEFT = 3,
        FIRE = 4,
    }
}
