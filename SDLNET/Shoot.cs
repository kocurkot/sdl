﻿using SdlDotNet.Core;
using SdlDotNet.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDLNET
{
     public class Shoot
    {
        public bool exists = false;
        public Rectangle r = new Rectangle();
        private Surface texture;
        public int x,y = 0;
        private float mleftover;
        public Boolean fireActive = false;
        private CursorKey actualDirection;
        Player player;
        public int points = 0;

        public float timmer = 0;

        public Shoot(Player player)
        {
            this.player = player;
        }
        public void init()
        {
            texture = new Surface(@"resources\fire2.png").Convert(SDLGame.m_VideoScreen, true, true);
            this.texture.Transparent = true;
            this.texture.TransparentColor = Color.FromArgb(255, 0, 255);  
        }

        public void logic(TickEventArgs tickArgs)
        {
            int offsetPerSecond = 300; // Przesunięcie pocisku w jednej sekundzie
            float offset = 1.0f / 1000 * tickArgs.TicksElapsed * offsetPerSecond;

            timmer += offset / offsetPerSecond;


            if (this.mleftover < 1)
            {
                offset += this.mleftover;
                this.mleftover = 0;
            }

            if (offset < 1)
            {
                this.mleftover += offset;
                offset = 0;
            }

            if (fireActive)
            {

                if (actualDirection == CursorKey.UP)
                {
                    y -= (int)offset;
                }
                else if (actualDirection == CursorKey.DOWN)
                {
                    y += (int)offset;
                }
                else if (actualDirection == CursorKey.LEFT)
                {
                    x -= (int)offset;
                }
                else if (actualDirection == CursorKey.RIGHT)
                {
                    x += (int)offset;
                }
                this.draw(x, y);

                if (isCollisionWithMapElements(actualDirection))
                {
                    System.Random random = new System.Random();
                    if (random.Next(1, 100) < 15)
                    {
                        if (actualDirection == CursorKey.UP)
                            actualDirection = CursorKey.DOWN;
                        else if (actualDirection == CursorKey.DOWN)
                            actualDirection = CursorKey.UP;
                        else if (actualDirection == CursorKey.LEFT)
                            actualDirection = CursorKey.RIGHT;
                        else if (actualDirection == CursorKey.RIGHT)
                            actualDirection = CursorKey.LEFT;
                    }
                    else
                        disableFire();
                }
                if (isCollisionWithOpponent(player.opponent, actualDirection))
                {
                    points++;
                    if (points == 10)
                    {
                        SDLGame.endOfGame = true;
                        SDLGame.won = player;
                    }

                    player.opponent.explosion();
                    disableFire();
                }
                if (isCollisionWithOpponent(player, actualDirection))
                {
                    points--;

                    player.explosion();

                    disableFire();
                }
            }              
        }

        private void disableFire()
        {
            fireActive = false;
            exists = false;
            x = -1; y = -1;
        }

        public void fire(CursorKey direction)
        {
            if (!exists && timmer > 0.7)
            {
                timmer = 0;

                if (direction == CursorKey.UP)
                {
                    x = player.x + 13;
                    y = player.y - 11;

                    exists = true;
                    actualDirection = CursorKey.UP;
                }
                else if (direction == CursorKey.DOWN)
                {
                    x = player.x + 13;
                    y = player.y + 30;

                    exists = true;
                    actualDirection = CursorKey.DOWN;
                }
                else if (direction == CursorKey.LEFT)
                {
                    x = player.x - 11;
                    y = player.y + 13;

                    exists = true;
                    actualDirection = CursorKey.LEFT;
                }
                else if (direction == CursorKey.RIGHT)
                {
                    x = player.x + 30;
                    y = player.y + 13;


                    exists = true;
                    actualDirection = CursorKey.RIGHT;
                }
                fireActive = true;
            }

        }

        public bool isCollisionWithOpponent(Player opponent, CursorKey direction)
        {
            Rectangle ghost = new Rectangle();
            if (direction == CursorKey.UP)
            {
                ghost = new Rectangle(new Point(x, y), new Size(6, 11));
            }
            else if (direction == CursorKey.DOWN)
            {
                ghost = new Rectangle(new Point(x, y), new Size(6, 11));
            }
            else if (direction == CursorKey.LEFT)
            {
                ghost = new Rectangle(new Point(x, y), new Size(11, 6));
            }
            else if (direction == CursorKey.RIGHT)
            {
                ghost = new Rectangle(new Point(x, y), new Size(11, 6)); ;
            }

            if(ghost.IntersectsWith(opponent.ghostElement))
                 return true;
            return false;
        }

        public int getGridPool(int number)
        {
            return (int)number / 32;
        }

        public bool isCollisionWithMapElements(CursorKey direction)
        {
            System.Random random = new System.Random();

            int tmpX = 0, tmpY = 0;
            if (direction == CursorKey.UP)
            {
                tmpX = getGridPool(this.x);
                tmpY = getGridPool(this.y - 1);
            }
            else if (direction == CursorKey.DOWN)
            {
                tmpX = getGridPool(this.x);
                tmpY = getGridPool(this.y + 6 + 1);
            }
            else if (direction == CursorKey.LEFT)
            {
                tmpX = getGridPool(this.x - 1);
                tmpY = getGridPool(this.y);
            }
            else if (direction == CursorKey.RIGHT)
            {
                tmpX = getGridPool(this.x + 6 + 1);
                tmpY = getGridPool(this.y);
            }
            if (tmpX < 0)
                tmpX = 0;
            if (tmpX > 24)
                tmpX = 24;
            if (tmpY < 0)
                tmpY = 0;
            if (tmpY > 17)
                tmpY = 17;

            if (SDLGame.map.grid[tmpY, tmpX] == 1)
                return true;

            return false;
        }

        public void draw(int x, int y)
        {
            int animX = 0, animY = 0;
            int sizeX = 0, sizeY = 0;
            if (actualDirection == CursorKey.UP)
            {
                sizeX = 6; sizeY = 11;
                animY = 0;
            }
            else if (actualDirection == CursorKey.DOWN)
            {
                sizeX = 6; sizeY = 11;
                animY += 17;
            }
            else if (actualDirection == CursorKey.LEFT)
            {
                sizeX = 11; sizeY = 6;
                animY += 28;
            }
            else if (actualDirection == CursorKey.RIGHT)
            {
                sizeX = 11; sizeY = 7;
                animY += 11;
            }
            SDLGame.m_VideoScreen.Blit(this.texture, new Point(this.x, this.y), new Rectangle(new Point(animX, animY), new Size(sizeX, sizeY)));
            //SDLGame.m_VideoScreen.Blit(this.texture, new Point(x, y));
            exists = true;                          
        }
    }
}
