﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using SdlDotNet.Graphics;
using SdlDotNet.Input;
using SdlDotNet.Core;
using SdlDotNet.Audio;

using SDLNET;

using Font = SdlDotNet.Graphics.Font;

public class SDLGame
{
    public static Surface m_VideoScreen;
    public static Font m_FpsFont;    

    public static ClassicMap map = new ClassicMap();

    public static Player[] players = new Player[2];

    public static bool endOfGame = false;
    public static Player won = null;

    public static Surface transparentBackground = null;
    public static Surface endText = null;

    public static void Main(string[] args)
    {
        m_VideoScreen = Video.SetVideoMode(800, 576, 32, false, false, false, true, true);

        restart();

        Events.TargetFps = 100;
        Events.Quit += new EventHandler<QuitEventArgs>(ApplicationQuitEventHandler);
        Events.Tick += new EventHandler<TickEventArgs>(ApplicationTickEventHandler);
        

        Keyboard.EnableKeyRepeat(200, 2);
        Events.KeyboardDown += new EventHandler<KeyboardEventArgs>(ApplicationKeyboardDownEventHandler);
        Events.KeyboardUp += new EventHandler<KeyboardEventArgs>(ApplicationKeyboardDownEventHandler);

        Events.Run();
    }

    private static void Initialize()
    {
        m_FpsFont = new Font("Arial.ttf", 12); // Font for displaying FPS counter      
        
        // Zmiana klawiszy sterowania dla drugiego gracza
        players[1].upKey = Key.W;
        players[1].downKey = Key.S;
        players[1].leftKey = Key.A;
        players[1].rightKey = Key.D;
        players[1].fireKey = Key.Tab;
        players[0].fireKey = Key.Slash;

        players[0].setOpponent(players[1]);
        players[1].setOpponent(players[0]);

    }

    public static void restart()
    {
        transparentBackground = new Surface(new Size(800, 600)).Convert(m_VideoScreen, true, true);
        transparentBackground.Alpha = 200;
        transparentBackground.AlphaBlending = true;
        transparentBackground.Fill(Color.Black);

        endOfGame = false;
        won = null;
        players[0] = new Player("Gracz#1",new Point(12 * 32,32 * 1), ref map);
        players[1] = new Player("Gracz#2", new Point(12 * 32, 32 * 16), ref map);
        Initialize();
        map.initialize();
        players[0].initialize((new Surface(@"resources\tank1.png")).Convert(SDLGame.m_VideoScreen, true, true));
        players[1].initialize((new Surface(@"resources\tank2.png")).Convert(SDLGame.m_VideoScreen, true, true));

    }

    private static void ApplicationKeyboardDownEventHandler(object sender, KeyboardEventArgs args)
    {
        // Wciśnięcie ESC wyłącza grę
        if (args.Key == Key.Escape){
            Events.QuitApplication();
        }

        if (!endOfGame)
        {
            foreach (var tmp in players)
            {
                tmp.movement(args);
            }
        }
        else
        {
            foreach (var tmp in players)
            {
                tmp.cursorKey[0] = false;
                tmp.cursorKey[1] = false;
                tmp.cursorKey[2] = false;
                tmp.cursorKey[3] = false;
                tmp.cursorKey[4] = false;
            }
            if (args.Key == Key.Return)
            {
                restart();
            }
        }
            
    }

    private static void ApplicationTickEventHandler(object sender, TickEventArgs args)
    {
        m_VideoScreen.Fill(Color.Black);

        map.draw();

        foreach (var tmp in players){
           
            tmp.logic(args);
            tmp.draw();
            tmp.shoot.logic(args);
        }
        m_VideoScreen.Blit(m_FpsFont.Render(String.Format("{0}: {1}", players[0].name, players[0].shoot.points), Color.Yellow), new Point(10, 8));
        m_VideoScreen.Blit(m_FpsFont.Render(String.Format("{0}: {1}", players[1].name, players[1].shoot.points), Color.Yellow), new Point(10, 600 - 47));

        if(endOfGame)
        {
            endText = m_FpsFont.Render(String.Format("KONIEC GRY - Wygrał: {0} - Naciśnij [ENTER] aby rozpocząć nową rozgrywkę", won.name), Color.Yellow);
            m_VideoScreen.Blit(transparentBackground, new Point(0,0));
            m_VideoScreen.Blit(endText, new Point(400 - endText.Width/2, 600/2));
        }


        m_VideoScreen.Update();
    }

    private static void ApplicationQuitEventHandler(object sender, QuitEventArgs args)
    {
        Events.QuitApplication();
    }
}